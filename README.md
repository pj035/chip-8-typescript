# Chip-8 Emulator written in TypeScript

## Debugging

There is a launch file inside `.vscode` folder that allows one to debug the application inside vscode running `ts-node`.

## Sources

* https://tobiasvl.github.io/blog/write-a-chip-8-emulator/

### Roms

* test_opcode.ch8: https://github.com/corax89/chip8-test-rom
* chip-8-test-rom.ch: https://github.com/metteo/chip8-test-rom
* c8_test.c8: https://github.com/Skosulor/c8int/tree/master/test
* IBM logo https://github.com/loktar00/chip8/blob/master/roms/IBM%20Logo.ch8
* other roms: https://github.com/taniarascia/chip8/tree/master/roms

## License

See [./LICENSE.txt](./LICENSE.txt)