import { readFileSync } from 'fs';

export function readRom(path: string) {
  return readFileSync(path);
}
