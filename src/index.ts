import { CPU, Display, Memory } from './hardware';
import { readRom } from './rom-loader';

function main() {
  const mem = new Memory();
  const display = new Display();

  // const rom = readRom('./roms/test_opcode.ch8');
  // const rom = readRom('./roms/c8_test.c8');
  const rom = readRom('./roms/chip8-test-rom.ch8');
  // const rom = readRom('./roms/IBM_logo.ch8');

  mem.loadRom(rom);

  const cpu = new CPU(mem, display);
  cpu
    .tick()
    .then(() => process.exit(0))
    .catch((e) => {
      console.error('CHIP-8 stopped', e);
      process.exit(1);
    });
}

void main();
