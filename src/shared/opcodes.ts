export enum EOpCode {
  ClearScreen = 0x00e0,
  SubroutineReturn = 0x00ee,

  // Non-Static OPCodes - these are only identified by first nibble

  Jump = 0x1000,
  SetRegister = 0x6000,
  AddValueToRegister = 0x7000,
  SetIndexRegister = 0xa000,
  Draw = 0xd000,
  SubroutineStart = 0x2000,
  Skip_3 = 0x3000,
  Skip_4 = 0x4000,
  Skip_5 = 0x5000,
  Skip_9 = 0x9000,
  LogArith_Instructions = 0x8000,
  JumpOffset = 0xb000,
  Random = 0xc000,
  // TODO: refactor this - how?
  F = 0xf000,
  SkipKey = 0xe000,
}

/**
 * Last nibble (N) of 8XYN instruction, which decides the acctual logical
 * or arithmetic instruction.
 * See https://tobiasvl.github.io/blog/write-a-chip-8-emulator/#logical-and-arithmetic-instructions
 */
export enum ELogicalArithmeticInstruction {
  /** Set VX to value of VY */
  Set = 0x0,
  /** VX is set to VX bitwise OR VY */
  OR = 0x1,
  /** VX is set to VX bitwise AND VY */
  AND = 0x2,
  /** VX is set to VX bitwise XOR VY  */
  XOR = 0x3,
  /** VX is set to VX + VY.
   * If result is larger then 0xf, then set VF to 1.
   * Otherwise set VF to 0.
   */
  Add = 0x4,
  /** Set VX to VX - VY.
   * Set VF to 1 if VX > VY - otherwise to 0
   */
  Subtract_VY = 0x5,
  /** Set VX to VY - VX
   * Set VF to 1 if VY > VX - otherwise to 0
   */
  Subtract_VX = 0x7,
  /** Shift VX one bit right. Set VF to 1 if bit was shifted out, otherwise 0. */
  Shift_Right = 0x6,
  /** Shift VX one bit left. Set VF to 1 if bit was shifted out, otherwise 0. */
  Shift_Left = 0xe,
}

/** Describes the NN of FXNN instruction. */
export enum EFInstructions {
  /** Set VX to current value of delay timer */
  SetVXDelayTimer = 0x07,
  /** Set delay timer to value of VX */
  SetDelayTimerVX = 0x15,
  /** Set sound timer to value of VX */
  SetSoundTimerVX = 0x18,
  /** Add VX to IndexRegister */
  AddIndex = 0x1e,
  /** Halts program and waits for key input */
  GetKey = 0x0a,
  /** IndexRegister is set to address of character stored in in VX' last nibble.  */
  Font = 0x29,
  BinaryCodedDecimalConversion = 0x33,
  LoadMemory = 0x65,
  StoreMemory = 0x55,
}
