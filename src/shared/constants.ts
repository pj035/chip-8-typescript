/** Screen dimensions in px. */
export const SCREEN = {
  width: 64,
  height: 32,
  /** Size of each character in screen px */
  font: {
    width: 4,
    height: 5,
  },
};

/** In Hz */
export const REFRESH_RATE = 60;

/** In Bytes*/
export const MEMORY_SIZE = 4096;
/** "It would expect a CHIP-8 program to be loaded into memory after it, starting at address 200 (512 in decimal)." */
export const MEMORY_ROM_START_ADDRESS = 0x200; // 512
/** Font starts at 0x050 address in memory.
 * > "For some reason, it’s become popular to put it at 050–09F [...]"
 */
export const MEMORY_FONT_START_ADDRESS = 0x050;

/** Chip-8 has 16 8-bit registers. */
export const NUMBER_OF_REGISTERS = 16;

/** Simulates a CPU cycle of X ms.
 * CPU execution will sleep for X ms.
 */
export const CPU_CYCLE = 10;
