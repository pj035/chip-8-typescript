/**
 * Array of tuples that maps keyboard codes
 * to CHIP-8 keys. 
 * Structure: [keyboard-code, chip-8key]
1	2	3	C
4	5	6	D
7	8	9	E
A	0	B	F
 */
export const KEYMAP = [
  [49, 0x1], // 1
  [50, 0x2], // 2
  [51, 0x3], // 3
  [52, 0xc], // 4
  [113, 0x4], // q
  [119, 0x5], // w
  [101, 0x6], // e
  [114, 0xd], // r
  [97, 0x7], // a
  [115, 0x8], // s
  [100, 0x9], // d
  [102, 0xe], // f
  [120, 0xa], // x
  [98, 0x0], // c
  [99, 0xb], // c
  [118, 0xf], // v
];

/** Contains the KEYMAP's keyboard codes in their character
 * representation.
 */
export const KEYBOARD_CODES_AS_STRING = KEYMAP.map((e) =>
  String.fromCharCode(e[0]).toLowerCase(),
);
