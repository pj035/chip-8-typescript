import {
  MEMORY_FONT_START_ADDRESS,
  MEMORY_ROM_START_ADDRESS,
  MEMORY_SIZE,
} from '../shared/constants';
import { FONT } from '../shared/font';

export class Memory {
  private buffer = new Uint8Array(MEMORY_SIZE);

  /** Returns the memory's byte value at specified address. */
  public read(address: number) {
    if (address < 0 || address > this.buffer.length - 1) {
      throw Error('MemorySize_Exceeded');
    }

    return this.buffer[address];
  }

  public write(address: number, value: number) {
    if (address < 0 || address > this.buffer.length - 1) {
      throw Error('MemorySize_Exceeded');
    }

    this.buffer[address] = value;
  }

  public loadRom(rom: Uint8Array) {
    if (rom.length > MEMORY_SIZE) {
      throw Error('MemorySize_Exceeded');
    }

    // Clear memory completely
    this.buffer = new Uint8Array(MEMORY_SIZE);
    // Load font
    for (let i = 0; i < FONT.length; i++) {
      this.buffer[MEMORY_FONT_START_ADDRESS + i] = FONT[i];
    }
    // Load ROM
    for (let i = 0; i < rom.length; i++) {
      this.buffer[MEMORY_ROM_START_ADDRESS + i] = rom[i];
    }
  }
}
