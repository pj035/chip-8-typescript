import { screen } from 'blessed';
// eslint-disable-next-line
const helpers: any = require('blessed').helpers;

import { SCREEN } from '../shared/constants';
import { KEYBOARD_CODES_AS_STRING, KEYMAP } from '../shared/keymap';

export class Display {
  private screen = screen({ smartCSR: true, debug: true });
  private buffer: number[][] = [[]];
  private color: string;
  private lastKey: number | null = null;

  constructor() {
    this.screen.title = 'Chip8';
    // eslint-disable-next-line
    this.color = helpers.attrToBinary({ fg: '#fff' }) as string;

    this.clearDisplay();
    // Quit on Escape, q, or Control-C.
    this.screen.key(['escape', 'z'], () => process.exit(0));
    this.screen.key(KEYBOARD_CODES_AS_STRING, (char: string, key) => {
      const keycode = char.charCodeAt(0);
      const chip8Key = KEYMAP.find((t) => t[0] === keycode);
      if (chip8Key) {
        this.lastKey = chip8Key[1];
      }
    });
  }

  /** Clears the whole display and framebuffer. */
  public clearDisplay() {
    const buf = [];
    for (let i = 0; i < SCREEN.width; i++) {
      const row = [];
      for (let j = 0; j < SCREEN.height; j++) {
        row[j] = 0;
      }
      buf.push(row);
    }

    this.buffer = buf;
    this.screen.clearRegion(0, SCREEN.width, 0, SCREEN.height);
    this.screen.render();
  }

  /**
   * Draws at position (x | y).
   * The value in the framebuffer at the given position will be flipped.
   * @returns if pixel had to be turned off due to flipping
   */
  public draw(x: number, y: number, value: number) {
    const flipPixel = this.buffer[x][y] === 1 && value === 1;

    if (flipPixel || value === 0) {
      this.screen.clearRegion(x, x + 1, y, y + 1);
      this.buffer[x][y] = 0;
    } else {
      /** full block unicode u+2588 if pixel is turned on */
      this.screen.fillRegion(this.color, '█', x, x + 1, y, y + 1);
      this.buffer[x][y] = 1;
    }

    this.screen.render();

    return flipPixel;
  }

  /** Returns last pressed keycode. */
  public get key() {
    return this.lastKey;
  }

  /** Sets last pressed keycode to null. */
  public set clearKey(_: string | null) {
    this.lastKey = null;
  }

  public debug(msg: string) {
    this.screen.debug(msg);
  }
}
