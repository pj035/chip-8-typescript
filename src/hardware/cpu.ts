import { Memory } from './memory';
import {
  EFInstructions,
  ELogicalArithmeticInstruction,
  EOpCode,
} from '../shared/opcodes';
import {
  CPU_CYCLE,
  MEMORY_FONT_START_ADDRESS,
  MEMORY_ROM_START_ADDRESS,
  NUMBER_OF_REGISTERS,
  REFRESH_RATE,
  SCREEN,
} from '../shared/constants';
import { Display } from './display';

// TODO: move to utils
const sleep = (ms: number) =>
  new Promise((resolve) => setInterval(() => resolve(true), ms));

export class CPU {
  private delayTimer = 0xff;
  private soundTimer = 0xff;
  /** Program Counter */
  private PC = MEMORY_ROM_START_ADDRESS;
  /** 16 8-bit registers. */
  private registers = new Uint8Array(NUMBER_OF_REGISTERS);
  // TODO: can we limit this properly to 16-bit?
  /** Chip-8's I register. */
  private indexRegister = 0;
  /**
   * Note: Arbitrary stack size. Original chip was using some part of memory.
   */
  private stack: number[] = [];
  /** If true, execution pauses. */
  private halted = false;

  constructor(private memory: Memory, private display: Display) {
    setInterval(() => {
      this.delayTimer -= 1;
      this.soundTimer -= 1;

      if (this.delayTimer === -1) {
        this.delayTimer = 0xff;
      }
      if (this.soundTimer === -1) {
        this.soundTimer = 0xff;
      }
    }, 1 / REFRESH_RATE);
  }

  /** Triggers CPU cycle. */
  public async tick(): Promise<void> {
    if (!this.halted) {
      const opcode = this.fetch();
      this.decode(opcode);
    }
    await sleep(CPU_CYCLE);
    return this.tick();
  }

  public fetch() {
    const first = this.memory.read(this.PC);
    const second = this.memory.read(this.PC + 1);
    this.incrementPC();
    // NOTE/impl: "An instruction is two bytes, so you will need to read two successive bytes from memory and combine them into one 16-bit instruction."
    // https://tobiasvl.github.io/blog/write-a-chip-8-emulator/#fetch
    // Shifting the first byte by 8 bit and combining it with logical or to the second
    // byte to create the 16 bit instruction.
    const opcode = (first << 8) | (second << 0);
    return opcode;
  }

  public decode(opcode: number) {
    // TODO: add check that it's a valid instruction (=16bit)

    this.debug(`Decode: ${opcode.toString(16)}`);

    // NOTE/impl: Masking off first hex to identify the instruction
    const type = opcode & 0xf000;

    /** 00E0: Clear screen */
    if (opcode === EOpCode.ClearScreen) {
      return this.display.clearDisplay();
    }
    /**00EE: Return from Subroutine */
    if (opcode === EOpCode.SubroutineReturn) {
      this.PC = this.stack.pop() as number;
      this.debug(`Subroutine returned to ${this.PC}`);
      return;
    }

    /** Second nibble of opcode */
    const X = (opcode & 0x0f00) % (NUMBER_OF_REGISTERS - 1);
    /** Third nibble of opcode */
    const Y = (opcode & 0x00f0) % (NUMBER_OF_REGISTERS - 1);
    /** 4-bit number, last nibble of opcode */
    const N = opcode & 0x000f;
    /** Second byte of opcode */
    const NN = opcode & 0x00ff;
    /** 12-bit address of opcode (last three nibbles) */
    const NNN = opcode & 0x0fff;
    /** Value in register X */
    const VX = this.registers[X];
    /** Value in register Y */
    const VY = this.registers[Y];

    switch (type) {
      /** 2NNN: Subroutines */
      case EOpCode.SubroutineStart: {
        this.stack.push(this.PC);
        this.PC = NNN;
        this.debug(`Subroutine start at ${this.PC.toString(16)}`);
        break;
      }
      /** 1NNN: Jump */
      case EOpCode.Jump: {
        this.PC = NNN;
        this.debug(`Jump PC to ${this.PC.toString(16)}`);
        break;
      }
      /** 6XNN: Set */
      case EOpCode.SetRegister: {
        this.registers[X] = NN;
        this.debug(`Set V${X} to ${NN.toString(16)}`);
        break;
      }
      /** 7XNN: Add */
      case EOpCode.AddValueToRegister: {
        this.registers[X] = VX + NN;
        this.debug(`Add V${X}: ${VX.toString(16)} + ${NN.toString(16)}`);
        break;
      }
      /** ANNN: Set index */
      case EOpCode.SetIndexRegister: {
        this.indexRegister = NNN;
        this.debug(`Set I to ${NNN.toString(16)}`);
        break;
      }
      /** DXYN: Display
       * Draw N pixel tall sprite from memory location at indexRegister at horizontal X in VX
       * and vertical Y in VY.
       * All "on" pixels in sprite (i.e. bit = 1) will flip the pixels on the screen.
       * If any pixel on display is turned off due to this VF is set to 1 - otherwise 0.
       *
       * > Each sprite consists of 8-bit bytes, where each bit corresponds to a horizontal pixel;
       * > sprites are between 1 and 15 bytes tall.
       * > They’re drawn to the screen by treating all 0 bits as transparent,
       * > and all the 1 bits will “flip” the pixels in the locations of the screen that it’s drawn to.
       *
       * https://tobiasvl.github.io/blog/write-a-chip-8-emulator/#display
       */
      case EOpCode.Draw: {
        // reset VF first. It will be set if pixels are erased.
        this.VF = 0;
        // Coordinates are modulo size of display
        // Sprites near edge should not be wrapped
        // NOTE/impl: Read N bytes from memory starting at indexRegister
        for (let n = 0; n < N; n++) {
          // NOTE/impl: stop drawing if we reach the bottom edge of screen
          if (VY + n >= SCREEN.height) {
            break;
          }
          const sprite = this.memory.read(this.indexRegister + n);
          for (let pos = 0; pos < 8; pos++) {
            // NOTE/impl: stop drawing this sprite if we reach right edge of screen
            if (VX + pos >= SCREEN.width) {
              break;
            }
            /* NOTE/impl: Loop from left to right and mask off all other bits except the one
              at pos. E.g. if sprite is 16 (dec) = 00001000, then we will get
               * pos = 0 => 00001000 & (00000001 << 7) => 00001000 & 10000000 => 0
               * pos = 1 => 00001000 & (00000001 << 6) => 00001000 & 01000000 => 0
               * pos = 2 => 00001000 & (00000001 << 5) => 00001000 & 00100000 => 0
               * pos = 3 => 00001000 & (00000001 << 4) => 00001000 & 00010000 => 0
               * pos = 4 => 00001000 & (00000001 << 3) => 00001000 & 00001000 => 1
               * pos = 5 => 00001000 & (00000001 << 2) => 00001000 & 00000100 => 0
               * pos = 6 => 00001000 & (00000001 << 1) => 00001000 & 00000010 => 0
               * pos = 7 => 00001000 & (00000001 << 0) => 00001000 & 00000001 => 0
            */
            const bit = sprite & (1 << (7 - pos)) ? 1 : 0;
            // Draw and set VF if necessary
            const x = (VX + pos) % SCREEN.width;
            const y = (VY + n) % SCREEN.width;
            if (this.display.draw(x, y, bit)) {
              this.VF = 1;
            }
          }
        }
        break;
      }
      /** 3XNN: Skip one instruction if VX is equal NN */
      case EOpCode.Skip_3: {
        if (VX === NN) {
          this.incrementPC();
        }
        break;
      }
      /** 4XNN: Skip one instruction if VX is NOT equal NN */
      case EOpCode.Skip_4: {
        if (VX !== NN) {
          this.incrementPC();
        }
        break;
      }
      /** 4XY0: Skip one instruction if VX is equal VY */
      case EOpCode.Skip_5: {
        if (VX === VY) {
          this.incrementPC();
        }
        break;
      }
      /** 9XY0: Skip one instruction if VX is NOT equal VY */
      case EOpCode.Skip_9: {
        if (VX !== VY) {
          this.incrementPC();
        }
        break;
      }
      /** 8XYN whereby the actual operation is determined by value in N. */
      case EOpCode.LogArith_Instructions: {
        switch (N) {
          case ELogicalArithmeticInstruction.Set: {
            this.registers[X] = VY;
            this.debug(`Set: V${X} to ${VY.toString(16)}`);
            break;
          }
          case ELogicalArithmeticInstruction.OR: {
            this.registers[X] = VX | VY;
            this.debug(`OR: Set V${X} to ${this.registers[X].toString(16)}`);
            break;
          }
          case ELogicalArithmeticInstruction.AND: {
            this.registers[X] = VX & VY;
            this.debug(`AND: Set V${X} to ${this.registers[X].toString(16)}`);
            break;
          }
          case ELogicalArithmeticInstruction.XOR: {
            this.registers[X] = VX ^ VY;
            this.debug(`XOR: Set V${X} to ${this.registers[X].toString(16)}`);
            break;
          }
          case ELogicalArithmeticInstruction.Add: {
            const sum = VX + VY;
            // NOTE/impl: Uint8Array sets the value to 0xff even in case of overflowing bits
            this.registers[X] = sum;
            this.VF = sum > 0xff ? 1 : 0;
            this.debug(
              `Add: Set V${X} to ${sum.toString(16)} and VF to ${this.VF}`,
            );
            break;
          }
          case ELogicalArithmeticInstruction.Subtract_VY: {
            // NOTE/impl: Uint8Array sets the value to 0xff even in case of overflowing bits
            this.registers[X] = VX - VY;
            this.VF = VX > VY ? 1 : 0;
            this.debug(
              `Subtract_VY: Set V${X} to ${this.registers[X].toString(
                16,
              )} and VF to ${this.VF}`,
            );
            break;
          }
          case ELogicalArithmeticInstruction.Subtract_VX: {
            // NOTE/impl: Uint8Array sets the value to 0xff even in case of overflowing bits
            this.registers[X] = VY - VX;
            this.VF = VY > VX ? 1 : 0;
            this.debug(
              `Subtract_VX: Set V${X} to ${this.registers[X].toString(
                16,
              )} and VF to ${this.VF}`,
            );
            break;
          }
          /** 8XY6: Shift right */
          case ELogicalArithmeticInstruction.Shift_Right: {
            this.registers[X] = VX >> 1;
            // NOTE/impl: if value mod 2 is 1, then the shifted out bit was 1
            this.VF = VX % 2 === 1 ? 1 : 0;
            this.debug(
              `Shift_Right: Set V${X} to ${this.registers[X].toString(
                16,
              )} and VF to ${this.VF}`,
            );
            break;
          }
          /** 8XYE: Shift left */
          case ELogicalArithmeticInstruction.Shift_Left: {
            this.registers[X] = VX << 1;
            // NOTE/impl: shift 8th bit to the most right place
            const shiftedBit = VX >> 7;
            this.VF = shiftedBit % 2 === 1 ? 1 : 0;
            this.debug(
              `Shift_Left: Set V${X} to ${this.registers[X].toString(
                16,
              )} and VF to ${this.VF}`,
            );
            break;
          }
        }
        break;
      }
      /** Support for BNNN. Jumps to NNN + V0 */
      case EOpCode.JumpOffset: {
        const V0 = this.registers[0];
        const jump = NNN + V0;
        this.PC = jump;
        this.debug(`Jump offset set PC to ${this.PC.toString(16)}`);
        // TODO: impl support for BXNN
        break;
      }
      /** CXNN: Random */
      case EOpCode.Random: {
        const random = Math.round(Math.random() * 255);
        this.registers[X] = random & NN;
        this.debug(`Set V${X} to ${this.registers[X].toString(16)}`);
        break;
      }
      /** EX9E and EXA1: Skip if key */
      case EOpCode.SkipKey: {
        if (NN === 0x9e) {
          if (this.display.key === VX) {
            this.incrementPC();
          }
        } else if (NN == 0xa1) {
          if (this.display.key !== VX) {
            this.incrementPC();
          }
        } else {
          this.halted = true;
          throw Error('Unsupported operation');
        }
        break;
      }
      /** FXNN */
      case EOpCode.F: {
        switch (NN) {
          /** FX07 */
          case EFInstructions.SetVXDelayTimer: {
            this.registers[X] = this.delayTimer;
            this.debug(`Set V${X} to ${this.registers[X].toString(16)}`);
            break;
          }
          /** FX15 */
          case EFInstructions.SetDelayTimerVX: {
            this.delayTimer = VX;
            this.debug(`Set delayTimer to ${VX.toString(16)}`);
            break;
          }
          /** FX18 */
          case EFInstructions.SetSoundTimerVX: {
            this.soundTimer = VX;
            this.debug(`Set soundTimer to ${VX.toString(16)}`);
            break;
          }
          /** FX1E */
          case EFInstructions.AddIndex: {
            this.indexRegister = VX;
            this.debug(`Set I to ${VX.toString(16)}`);
            break;
          }
          /** Awaits key input. */
          case EFInstructions.GetKey: {
            // NOTE/impl: decrement counter until key is actually pressed
            if (this.display.key === null) {
              this.decrementPC();
            } else {
              this.registers[X] = this.display.key;
              this.display.clearKey = null;
              this.debug(`Set V${X} to ${this.registers[X].toString(16)}`);
            }
            break;
          }
          /** FX29: Font Character */
          case EFInstructions.Font: {
            /* NOTE/impl: each font character is stored as 5 byte values in FONT
             *  Therefore the address in memory of the character's first byte is
             *  MEMORY_FONT_START_ADDRESS + character hex value * 5
             */
            const char = VX & 0xf;
            const fIndex = char * 5;
            this.indexRegister = MEMORY_FONT_START_ADDRESS + fIndex;
            this.debug(
              `Set FONT ${char.toString(16)} to I ${this.indexRegister.toString(
                16,
              )}`,
            );
            break;
          }
          /** FX33: Binary-coded decimal conversion */
          case EFInstructions.BinaryCodedDecimalConversion: {
            // NOTE/impl: op takes VX and converts number into three decimal digits
            // e.g. 156 (0x9C) is converted to 1 5 9 and stored at I I+1 I+2
            const values = [
              Math.floor(VX / 100),
              Math.floor(VX / 10) % 10,
              VX % 10,
            ];
            values.forEach((val, index) => {
              this.memory.write(this.indexRegister + index, val);
            });
            this.debug(
              `binary conversion of ${VX} to ${values.join(
                ',',
              )} and stored in ${this.indexRegister}`,
            );
            break;
          }
          /** FX55: Store Memory */
          case EFInstructions.StoreMemory: {
            for (let i = 0; i <= VX; i++) {
              this.memory.write(this.indexRegister + i, this.registers[i]);
            }
            break;
          }
          /** FX65: Load Memory */
          case EFInstructions.LoadMemory: {
            for (let i = 0; i <= VX; i++) {
              this.registers[i] = this.memory.read(this.indexRegister + i);
            }
            break;
          }
        }
        break;
      }
      default: {
        // throw Error('Unsupported opcode');
      }
    }
  }

  /** Set value in VF. */
  private set VF(value: number) {
    this.registers[NUMBER_OF_REGISTERS - 1] = value;
    this.debug(`Set VF to ${value.toString(16)}`);
  }

  private get VF(): number {
    return this.registers[NUMBER_OF_REGISTERS - 1];
  }

  /** Moves PC to next instruction. */
  private incrementPC() {
    this.PC += 2;
    this.debug(`Set PC to ${this.PC.toString(16)}`);
  }

  /** Moves PC to previous instruction. */
  private decrementPC() {
    this.PC -= 2;
    this.debug(`Set PC to ${this.PC.toString(16)}`);
  }

  private debug(msg: string) {
    this.display.debug(`[CPU] ${msg}`);
  }
}
